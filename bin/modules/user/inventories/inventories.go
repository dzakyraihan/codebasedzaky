package inventories

import (
	"context"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Inventory struct {
	Nama     string `json:"nama" bson:"nama"`
	Kode     string `json:"kode" bson:"kode"`
	Harga    int    `json:"harga" bson:"harga"`
	Stock    int    `json:"stock" bson:"stock"`
	Kategori string `json:"kategori" bson:"kategori"`
}

func AddInventory(inventory Inventory) error {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://localhost:8000"))
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(context.TODO())

	collection := client.Database("namamasingmasing").Collection("inventories")
	_, err = collection.InsertOne(context.TODO(), inventory)
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}
