package routegroup

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/dzakyraihan/backend-codebase-go-fork-dzaky/modules/inventories/inventories"
)

type ErrorResponse struct {
	Message string `json:"message"`
}

func AddInventoryHandler(w http.ResponseWriter, r *http.Request) {
	var inventory inventories.Inventory
	err := json.NewDecoder(r.Body).Decode(&inventory)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(ErrorResponse{Message: "Invalid request payload"})
		return
	}

	err = inventories.AddInventory(inventory)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(ErrorResponse{Message: "Failed to add inventory"})
		return
	}

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(inventory)
}

func GetInventoryByCodeHandler(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	code := params["code"]
	inventory := inventories.Inventory{
		Nama:     "Barang 1",
		Kode:     code,
		Harga:    100,
		Stock:    50,
		Kategori: "Kategori A",
	}

	json.NewEncoder(w).Encode(inventory)
}
